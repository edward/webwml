#use wml::debian::translation-check translation="7fbf113ef094837f72d2bdb71154488accfe2afb" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В почтовом сервере Dovecot было обнаружено несколько
уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12100">CVE-2020-12100</a>

    <p>Получение письма с вложенными друг в друга MIME-частями приводит к
    исчерпанию ресурсов, так как Dovecot пытается выполнить их грамматическую разборку.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12673">CVE-2020-12673</a>

    <p>Реализация NTLM в Dovecot неправильно выполняет проверку размера буфера сообщения,
    что приводит к аварийным остановкам при чтении за пределами выделенного буфера памяти.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12674">CVE-2020-12674</a>

    <p>Реализация механизм RPA в Dovecot принимает сообщения нулевой длины,
    что позже приводит к аварийной остановке при обработке утверждения.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 1:2.3.4.1-5+deb10u3.</p>

<p>Рекомендуется обновить пакеты dovecot.</p>

<p>С подробным статусом поддержки безопасности dovecot можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4745.data"
