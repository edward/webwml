#use wml::debian::template title="Auditoria para mantenedores(as) de pacotes"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="534d1b782cfb92f46dc41fd064f779fffc329b12"

<p>Se você é um(a) mantenedor(a) de um pacote que está contido no
repositório do Debian, por favor considere examinar o código por si
mesmo(a).</p>

<p>A disponibilidade de <a href="tools">ferramentas de auditoria de código-fonte</a>
pode facilitar esse processo significativamente; mesmo que você não tenha tempo
de fazer uma auditoria minuciosa você mesmo(a), você poderá encontrar áreas
que são potencialmente problemáticas.</p>

<p>Se necessitar de auxílio, por favor contate ou o
<a href="$(HOME)/security/#contact">time de segurança do Debian</a> , ou a
<a href="https://lists.debian.org/debian-security/">lista (pública) de discussão debian-security</a>
para ajuda em como conduzir uma auditoria de código-fonte.</p>

# considere juntar-se à <a
#href="http://shellcode.org/mailman/listinfo/debian-audit">lista de discussão
#debian-audit</a> e peça para um(a) voluntário(a) examinar seu
#pacote.</p>

<h2>Fontes para mantenedores(as)</h2>

<p>Mantenedores(as) que desejem revisar o código-fonte podem estar
interessados(as) em ler o artigo (em inglês) da Debconf6 <a
href="https://people.debian.org/~jfs/debconf6/security/weeding_security_bugs.pdf">Erradicando
bugs de segurança no Debian</a> (Weeding out security bugs in Debian)
(<a href="https://people.debian.org/~jfs/debconf6/security/slides/weeding_security_bugs.slides.pdf">slides</a>)
ou as notas (em inglês) <a
href="https://people.debian.org/~jfs/debconf6/security/common-problems.txt">Uma
curta e prática visão geral de como encontrar alguns erros comuns em programas
escritos em várias linguagens</a> (Short, practical overview on how to find a
few common mistakes in programs written in various languages).
</p>

<p>O artigo <q>Erradicando bugs de segurança no Debian</q> foi apresentado na
Debconf6 no México e foi parte de um workshop. Para mantenedores(as) novos(as)
em auditoria,
<a href="https://people.debian.org/~jfs/debconf6/security/samples/">os
exemplos de código</a> e os <a
href="http://meetings-archive.debian.net/pub/debian-meetings/2006/debconf6/">vídeos
do workshop</a> podem ser úteis.</p>

<h2>Novos lançamentos</h2>

<p>Como parte de ser um(a) mantenedor(a) responsivo(a), você também deve ficar
de olho nos novos lançamentos do(a) autor(a) do software do seu pacote. Se
o arquivo de registro de alterações (changelog) menciona um problema de
segurança, você deve tentar observar se você tem uma versão do código na
versão estável (stable) que é vulnerável.</p>

<p>Se você tem uma versão vulnerável disponível na versão estável (stable),
por favor contate o time de segurança - como descrito <a
href="$(HOME)/security/faq">no FAQ do time de segurança</a>.</p>