#use wml::debian::template title="MiniHOWTO para gestores(as) de candidaturas de novos(as) membros(as) (AM)"
#use wml::debian::translation-check translation="c3a3b8986d2ffef95ef7bb8f7d99f36678ff0e8f"

<p>
<b>Nota:</b> a página wiki
<a href="https://wiki.debian.org/FrontDesk/AMTutorial">tutorial do(a) gestor(a)
de candidatura (AM)</a> está mais atualizada que está página.
</p>

<h2>Documentação e infraestrutura para gestor(a) de candidatura</h2>

<p>A informação básica necessária pelo(a) gestor(a) de candidatura é fornecida
aqui, no <a href="newmaint">canto novos(as) membros(as)</a>. Comece
olhando até ficar familiarizado(a) com o processo e todos os requisitos para
os(as) candidatos(as).</p>

<p>Existem três endereços de e-mail importantes para o(a) gestor(a) de
candidatura:</p>

<dl>
 <dt>A lista de discussão de novos(as) membros(as):<a href="https://lists.debian.org/debian-newmaint">
  debian-newmaint@lists.debian.org</a></dt>
  <dd>Esta lista de discussão cobre todos os aspectos do processo de novos(as)
   membros(as) e é usada pelo grupo de novos(as) membros(as) (secretaria,
   gestores(as) de candidatura, gestores(as) de contas Debian) e outros(as)
   para discutir problemas administrativos e o processo de novos(as) membros(as).
   Se você tiver alguma dúvida sobre o processo de NM, pode pedir ajuda lá. Por
   favor observe que a lista é arquivada publicamente, portanto, questões de
   natureza altamente pessoal não deve ser discutidas lá.
   Em vez disso, você pode perguntar à recepção em privado.</dd>

 <dt>A secretaria de novos(as) membros(as): nm@debian.org</dt>
  <dd>Este é o endereço para onde o início das candidaturas, as mensagens
   advogando e os relatórios finais de candidaturas são enviados. Qualquer
   pergunta pessoal sobre candidatura individual que seja inadequada para
   um fórum público também devem ser direcionada para cá.</dd>

 <dt>Os(As) gestor(as) de contas Debian (DAMs): da-manager@debian.org</dt>
  <dd>Normalmente esse endereço é importante apenas para enviar o relatório
   final da candidatura. Os DAMs são responsáveis por criar novas contas nas
   máquinas Debian e adicionar as chaves OpenPGP de novos(as) membros(as) ao
   chaveiro. Eles também tomam a decisão final sobre cada candidatura, como
   delegados oficiais do líder do projeto Debian para novos(as)
   membros(as).</dd>
</dl>

<p>A coordenação do processo de NM ocorre em <url "https://nm.debian.org/"/>,
onde um site web fornece uma interface para um banco de dados contendo todas as
informações importantes sobre a candidatura de NM.
O(A) candidato(a) pode usar o site para acompanhar o status de sua candidatura
e o(a) gestor(a) de candidatura podem usá-lo para organizar seu trabalho.</p>

<p>Como gestor(a) de candidatura você pode fazer login através de uma
conexão https segura, mas observe que a senha usada no nm.debian.org
<em>não</em> é a senha usada na sua conta Debian normal (a menos que você
altere-as para serem as mesmas, mas isso é de sua responsabilidade). Você pode
registrar o que fez com um(a) candidato(a) e com quantos(as) candidatos(as)
deseja trabalhar ao mesmo tempo.</p>

<p>Para colocar alguém em suspensão, você precisa acessar a página de status
do(a) candidato(a) depois de fazer login e marcar a opção "No" em "AM approves
and submits report". Você também deve colocar uma linha no campo de comentário
do(a) AM para documentar porque você fez isso.</p>

<p>O restante das páginas é bastante autoexplicativo. Algumas estatísticas
sobre todos os(as) gestores(as) de candidatura estão disponíveis, você pode ver
uma lista não classificada de todos(as) os(as) candidatos(as) e alterar seu
perfil de AM.</p>

<h2>Notas sobre as verificações do(a) NM</h2>

<p>Como a documentação orientada ao(a) NM já fornece informações suficientes
sobre os requisitos das verificações, nada disso será repetido aqui. Se você
não tem certeza como gerenciar um(a) candidato(a), use os excelentes modelos
fornecidos pelo projeto
<a href="https://salsa.debian.org/nm-team/nm-templates">nm-templates</a> do
Joerg Jaspert. As perguntas devem ser feitas em debian-newmaint@l.d.o ou
enviadas à recepção.</p>

<h3>Colocando uma candidatura em suspensão</h3>

<p>O(A) candidato(a) que não pode ou não está disposto(a) a investir tempo
suficiente nas verificações de novo(a) membro(a) para concluí-las em um período
de tempo razoável (&sim; 6 a 9 meses) deve ser colocado(a) em suspensão. Isso
não é um problema ou uma penalização das habilidades do(a) candidato(a), mas uma
reação simples à falta de tempo. Muitas pessoas querem ingressar no Debian,
portanto, o(a) candidato(a) não deve bloquear a disponibilidade do AM.</p>

<p>Você deve discutir a possibilidade de colocar uma candidatura em suspensão
quando tiver a sensação de que ela não está avançando, porque o(a) candidato(a)
não responde ou porque a única resposta dele(a) é "Ah, farei isso em breve".
Enfatize o fato de que não é problema sair da suspensão quando tiver mais
tempo.</p>

<h3>Outras notas importantes</h3>

<ul>
 <li>O(A) candidato(a) deve fornecer uma biografia curta que pode ser enviada
  para debian-project@lists.debian.org em um e-mail normal "Novo(a) membro(a)".
  Isso é muito útil para introduzir um(a) novo(as) membro no projeto.
  Por favor observe que o(a) candidato(a) <em>tem</em> de concordar com a
  publicação desta biografia.</li>

 <li>Obtenha algumas informações sobre o histórico do(a) candidato(a) e
  coloque-as no relatório &mdash; use seu mecanismo de pesquisa favorito,
  arquivos de mensagens privadas, o BTS e todos os outros meios que lhe vierem
  à mente.
  Às vezes, o processo de candidatura é muito curto para obter uma impressão
  adequada da personalidade do(a) candidato(a), portanto, tente descobrir o
  que ele(a) fez no passado.</li>

 <li>Peça aos(as) padrinhos/madrinhas e outras pessoas que trabalharam
  juntos(as) com o(a) candidato(a) que forneçam breves declarações sobre ele(a).
  À medida que mais e mais pacotes são mantidos em equipe, você quase sempre
  encontra alguém capaz de lhe falar mais sobre um(a) candidato(a).
  Inclua essas declarações no relatório.</li>

 <li>Ao verificar o desempenho de um(a) candidato(a) que faz empacotamento,
  você deve estar ciente de que um pacote minúsculo no repositório não é
  suficiente para satisfazer a parte de habilidades nas verificações de "tarefas
  e habilidades". O pacote deveria ter mais de um upload, alguns(mas)
  usuários(as) (verifique o popcon) e alguns relatórios de bugs (se possível,
  fechados). Isso é importante para ver como um(a) candidato(a)
  interage com os(as) usuários(as).</li>
</ul>
