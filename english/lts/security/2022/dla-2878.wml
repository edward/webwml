<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that roundcube, a skinnable AJAX based webmail
solution for IMAP servers, did not properly sanitize HTML
messages. This would allow an attacker to perform Cross-Site Scripting
(XSS) attacks.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.2.3+dfsg.1-4+deb9u10.</p>

<p>We recommend that you upgrade your roundcube packages.</p>

<p>For the detailed security status of roundcube please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/roundcube">https://security-tracker.debian.org/tracker/roundcube</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2878.data"
# $Id: $
