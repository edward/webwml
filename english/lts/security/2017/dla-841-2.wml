<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2016-8743">CVE-2016-8743</a> 
introduced a regression which would segfault
apache workers under certain conditions (#858373), an issue similar to
previously fixed <a href="https://security-tracker.debian.org/tracker/CVE-2015-0253">CVE-2015-0253</a>.</p>

<p>The issue was introduced in DLA-841-1 and the associated
2.2.22-13+deb7u8 package version. For Debian 7 <q>Wheezy</q>, these
problems have been fixed in version 2.2.22-13+deb7u11.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-841-2.data"
# $Id: $
