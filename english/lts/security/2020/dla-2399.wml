<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities have been discovered in packagekit, a package
management service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16121">CVE-2020-16121</a>

    <p>Vaisha Bernard discovered that PackageKit incorrectly handled
    certain methods. A local attacker could use this issue to learn the
    MIME type of any file on the system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16122">CVE-2020-16122</a>

    <p>Sami Niemimäki discovered that PackageKit incorrectly handled local
    deb packages. A local user could possibly use this issue to install
    untrusted packages, contrary to expectations.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.1.5-2+deb9u2.</p>

<p>We recommend that you upgrade your packagekit packages.</p>

<p>For the detailed security status of packagekit please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/packagekit">https://security-tracker.debian.org/tracker/packagekit</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2399.data"
# $Id: $
