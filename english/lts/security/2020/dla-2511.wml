<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in highlight.js, a JavaScript library for syntax
highlighting. If a website or application renders user provided data it
might be affected by a Prototype Pollution. This might result in strange
behavior or crashes of applications that do not correctly handle unknown
properties.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
8.2+ds-5+deb9u1.</p>

<p>We recommend that you upgrade your highlight.js packages.</p>

<p>For the detailed security status of highlight.js please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/highlight.js">https://security-tracker.debian.org/tracker/highlight.js</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2511.data"
# $Id: $
