<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an external entity expansion vulnerability
in jackson-databind, a Java library for processing JSON.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25649">CVE-2020-25649</a></li>
</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
2.8.6-1+deb9u8.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2406.data"
# $Id: $
