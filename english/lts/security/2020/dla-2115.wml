<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a use-after-free vulnerability in in the
<tt>proftpd-dfsg</tt> FTP server.</p>

<p>Exploitation of this vulnerability within the memory pool handling
could have allowed a remote attacker to execute arbitrary code on the
affected system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9273">CVE-2020-9273</a>

    <p>In ProFTPD 1.3.7, it is possible to corrupt the memory pool by
    interrupting the data transfer channel. This triggers a use-after-free in
    <tt>alloc_pool</tt> in <tt>pool.c</tt>, and possible remote code
    execution.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.5e+r1.3.5-2+deb8u6.</p>

<p>We recommend that you upgrade your proftpd-dfsg packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2115.data"
# $Id: $
