<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in wpa, a set of tools to support WPA and WPA2
(IEEE 802.11i).
Missing validation of data can result in a buffer over-write, which might
lead to a DoS of the wpa_supplicant process or potentially arbitrary code
execution.</p>

<p>On request, together with this upload support for WPA-EAP-SUITE-B(-192)
has been enabled.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
2:2.4-1+deb9u8.</p>

<p>We recommend that you upgrade your wpa packages.</p>

<p>For the detailed security status of wpa please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wpa">https://security-tracker.debian.org/tracker/wpa</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2572.data"
# $Id: $
