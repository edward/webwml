<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in libsamplerate, an audio sample rate conversion
library. Using a crafted audio file a buffer over-read might happen in
calc_output_single() in src_sinc.c.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
0.1.8-8+deb9u1.</p>

<p>We recommend that you upgrade your libsamplerate packages.</p>

<p>For the detailed security status of libsamplerate please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libsamplerate">https://security-tracker.debian.org/tracker/libsamplerate</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2845.data"
# $Id: $
