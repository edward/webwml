<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Chrony, a versatile implementation of the
Network Time Protocol, did not verify peer associations of symmetric
keys when authenticating packets, which might allow remote attackers
to conduct impersonation attacks via an arbitrary trusted key, aka a
"skeleton key."</p>

<p>This update also resolves Debian bug #568492.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.24-3.1+deb7u4.</p>

<p>We recommend that you upgrade your chrony packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-742.data"
# $Id: $
