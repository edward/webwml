<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Some minor security issues have been identified and fixed in binutils in
Debian LTS. These are:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2226">CVE-2016-2226</a>

    <p>Exploitable buffer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4487">CVE-2016-4487</a>

    <p>Invalid write due to a use-after-free to array btypevec.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4488">CVE-2016-4488</a>

    <p>Invalid write due to a use-after-free to array ktypevec.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4489">CVE-2016-4489</a>

    <p>Invalid write due to integer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4490">CVE-2016-4490</a>

    <p>Write access violation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4492">CVE-2016-4492</a>

    <p>Write access violations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4493">CVE-2016-4493</a>

    <p>Read access violations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6131">CVE-2016-6131</a>

    <p>Stack buffer overflow when printing bad bytes in Intel Hex objects</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.22-8+deb7u3.</p>

<p>We recommend that you upgrade your binutils packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-552.data"
# $Id: $
