<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Dominic Scheirlinck and Scott Geary of Vend reported an insecure
behaviour in the lighttpd web server. Lighttpd assigned Proxy header
values from client requests to internal HTTP_PROXY environment
variables. This could be used to carry out Man in the Middle Attacks
(MIDM) or create connections to arbitrary hosts.</p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in version
1.4.31-4+deb7u5.</p>

<p>We recommend that you upgrade your lighttpd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-583.data"
# $Id: $
