<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of proftpd-dfsg issued as DLA-1753-1 caused a regression
when using the sftp module. Login to the sftp server was impossible
when the SFTPPAMEngine option was turned on (#926719).</p>

<p>This update reverts to upstream version 1.3.5 again since even the
latest upstream release 1.3.6 is still affected by different sftp
related bugs (#927270). All fixes for the memory leaks were backported
separately now.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.3.5e+r1.3.5-2+deb8u1.</p>

<p>We recommend that you upgrade your proftpd-dfsg packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1753-2.data"
# $Id: $
