#use wml::debian::template title="Debian GNU/kFreeBSD"

#use wml::debian::translation-check translation="6f3adf6374f35194686f89dec2ba66b1ecf3bb5f"
#use wml::debian::toc

<toc-display/>

<p>Debian GNU/kFreeBSD to adaptacja składająca się z
<a href="https://www.gnu.org/">programów GNU trybu użytkownika</a> używających
<a href="https://www.gnu.org/software/libc/">biblioteki języka C GNU</a> działających pod kontrolą jądra
<a href="https://www.freebsd.org/">FreeBSD</a>, w połączeniu ze standardowym
<a href="https://packages.debian.org/">zestawem pakietów Debiana</a>.</p>

<div class="important">
<p>Debian GNU/kFreeBSD nie jest oficjalnie wspieraną architekturą. 
Zostało wydane wraz z Debianem 6.0 (Squeeze) i 7.0 (Wheezy) jako "pokaz przedpremierowy" i pierwsza adaptacja dla jądra innego niż Linux. Aczkolwiek od wersji Debian 8 (Jessie) 
nie jest on już zawarty w oficjalnych wydaniach.</p>
</div>

<toc-add-entry name="resources">Zasoby</toc-add-entry>

<p>Więcej informacji na temat tej adaptacji (w tym FAQ) znajduje się na stronie wiki
<a href="https://wiki.debian.org/Debian_GNU/kFreeBSD">Debian GNU/kFreeBSD</a>.</p>

<h3>Listy dyskusyjne</h3>
<p><a href="https://lists.debian.org/debian-bsd">Lista dyskusyjna Debian GNU/k*BSD</a>.</p>

<h3>IRC</h3>
<p><a href="irc://irc.debian.org/#debian-kbsd">Kanał IRC #debian-kbsd</a> (na irc.debian.org).</p>

<toc-add-entry name="Development">Rozwój</toc-add-entry>

<p>Ponieważ używamy Glibc, problemy z przenośnością są bardzo proste, i w
większości przypadków ograniczają się do skopiowania testu konfiguracji z
innego systemu opartego na Glibc (np. GNU lub GNU/Linux). Więce szczegółów
można znaleźć w dokumencie na temat
<a href="https://salsa.debian.org/bsd-team/web/raw/master/porting/PORTING">adaptacji</a>.</p>

<p>Więcej szczegółów na temat tego co trzeba jeszcze zrobić znajduje się w
<a href="https://salsa.debian.org/bsd-team/web/raw/master/TODO">TODO</a>.</p>

<toc-add-entry name="availablehw">Sprzęt dostępny dla developerów Debiana</toc-add-entry>

<p>lemon.debian.net (kfreebsd-amd64) jest
dostępny dla developerów Debiana do pracy nad adaptacją.
Więcej informacji na temat tych maszyn znajduje się w
<a href="https://db.debian.org/machines.cgi">bazie danych maszyn</a>.
Dostępne są dwa środowiska chroot: testing oraz unstable.
Te systemy nie są administrowane przez DSA, więc <b>nie należy wysyłać
próśb związanych z nimi na adres debian-admin</b>. Zamiast tego użyj
<email "admin@lemon.debian.net">.</p>
