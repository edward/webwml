#use wml::debian::translation-check translation="ac644b9d3f1ffa53a127bad7456d7f79b9fcb40c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Brève description</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1152">CVE-2018-1152</a>

<p>Vulnérabilité de déni de service causée par une division par zéro lors du
traitement d’une image BMP contrefaite dans TJBench.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14498">CVE-2018-14498</a>

<p>Déni de service (lecture hors limites de tampon de tas et plantage
d'application) à l'aide d'une image 8 bits contrefaite dans laquelle un ou plusieurs
indices de couleur sont en dehors de l’intervalle des numéros des entrées de
palette.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13790">CVE-2020-13790</a>

<p>Lecture hors limites de tampon de tas à l'aide d'un fichier d’entrée PPM mal
formé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14152">CVE-2020-14152</a>

<p>Fonction jpeg_mem_available() ne respectant pas le réglage max_memory_to_use,
pouvant provoquer une consommation excessive de mémoire.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:1.5.1-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libjpeg-turbo.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libjpeg-turbo, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libjpeg-turbo">https://security-tracker.debian.org/tracker/libjpeg-turbo</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2302.data"
# $Id: $
