#use wml::debian::translation-check translation="dc69d767e027bfe397aa881215364b2a757248dd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans python-apt, une interface de
Python pour libapt-pkg.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15795">CVE-2019-15795</a>

<p>Il a été découvert que python-apt utilise encore les hachages MD5 pour
valider certains paquets téléchargés. Un attaquant distant, capable de
réaliser une attaque de type « homme du milieu », pourrait utiliser ce défaut
pour installer des paquets modifiés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15796">CVE-2019-15796</a>

<p>Il a été découvert que python-apt pourrait installer des paquets à partir de
dépôts non fiables, contrairement aux attentes.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.9.3.13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-apt.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2074.data"
# $Id: $
