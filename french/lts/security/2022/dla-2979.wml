#use wml::debian::translation-check translation="5e12f6f5ec77d6fecead06217c81711b42148bac" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un défaut a été découvert dans usbguard, un cadriciel pour une politique
d'autorisation de périphériques USB. Lors de l'utilisation du démon
usbguard-dbus, un utilisateur non privilégié pouvait faire qu'USBGuard
permettait à des périphériques USB d'être connectés dans le futur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
0.6.2+ds1-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets usbguard.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de usbguard, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/usbguard">\
https://security-tracker.debian.org/tracker/usbguard</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2979.data"
# $Id: $
