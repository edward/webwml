#use wml::debian::translation-check translation="47fb1e161b11f7c3b8e0833cd6fbbf8712a02fa7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le paquet ieee-data, qui fournit les listages des identifiants OUI et IAB
assignés par l’IEEE Standards Association, propose un script (update-ieee-data)
qui interroge ieee.org pour télécharger les ensembles de données les plus
récents et les enregistrer dans /var/lib/ieee-data/.</p>

<p>Ce script n’était plus valable pour Stretch depuis la fin de l’année 2018
quand l’URL a changé.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 20160613.1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ieee-data.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ieee-data, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ieee-data">\
https://security-tracker.debian.org/tracker/ieee-data</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2703.data"
# $Id: $
