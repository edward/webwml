#use wml::debian::translation-check translation="5b13047cdb5f012f91bfc605c6a1ec89dd96ddec" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La DLA-2836-1 a été diffusée, corrigeant le <a
href="https://security-tracker.debian.org/tracker/CVE-2021-43527">CVE-2021-43527</a>
dans nss, mais cela menait à une régression, empêchant les connexions SSL
dans Chromium. Le rapport de bogue complet peut être consulté à cette
adresse :
<a href="https://bugs.debian.org/1001219.">https://bugs.debian.org/1001219.</a></p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2:3.26.2-1.1+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nss.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nss, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nss">\
https://security-tracker.debian.org/tracker/nss</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2836-2.data"
# $Id: $
