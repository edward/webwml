#use wml::debian::translation-check translation="5307642c01ac5a739337b881046815b6c18b6443" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans libimage-exiftool-perl, une
bibliothèque et un programme pour lire et écrire des méta-informations dans des
fichiers multimédia. Elle pourrait avoir pour conséquence l'exécution de code
arbitraire lors du traitement d'un fichier DjVu mal formé.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 10.40-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libimage-exiftool-perl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libimage-exiftool-perl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libimage-exiftool-perl">\
https://security-tracker.debian.org/tracker/libimage-exiftool-perl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2663.data"
# $Id: $
