#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le navigateur
web Firefox de Mozilla : plusieurs erreurs de sécurité de la mémoire,
dépassements de tampon et autres erreurs d'implémentation pourraient
conduire à l'exécution de code arbitraire ou au contournement de la
politique de même origine.</p>

<p>Une attaque de type <q>homme du milieu</q> dans le mécanisme de mise
à jour des greffons a été corrigée.</p>

<p>Une vulnérabilité d'utilisation de mémoire après libération dans
l'animation de SVG a été découverte, permettant à un attaquant distant de
provoquer un déni de service (plantage de l'application) ou d'exécuter du
code arbitraire, si un utilisateur est piégé dans l'ouverture d'un site web
contrefait pour l'occasion.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 45.5.1esr-1~deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-730.data"
# $Id: $
