#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Nikolay Ermishkin de l'équipe de sécurité de Mail.Ru et Stewie ont
découvert plusieurs vulnérabilités dans ImageMagick, un ensemble de
programmes pour manipuler des images. Ces vulnérabilités, connues
collectivement sous le nom de ImageTragick, sont la conséquence de l'absence
de vérification des entrées non fiables. Un attaquant doté du contrôle sur
l'image d'entrée pourrait, avec les droits de l'utilisateur se servant de
l'application, exécuter du code
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3714">CVE-2016-3714</a>), faire des requêtes HTTP
GET ou FTP (<a href="https://security-tracker.debian.org/tracker/CVE-2016-3718">CVE-2016-3718</a>),
ou supprimer (<a href="https://security-tracker.debian.org/tracker/CVE-2016-3715">CVE-2016-3715</a>), déplacer
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3716">CVE-2016-3716</a>), ou lire
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-3717">CVE-2016-3717</a>) des fichiers locaux.</p>

<p>Ces vulnérabilités sont particulièrement critiques si ImageMagick traite
des images venant de parties distantes, par exemple d'une partie d'un
service web.</p>

<p>La mise à jour désactive les codeurs vulnérables (EPHEMERAL, URL, MVG,
MSL et PLT) et les lectures indirectes par l'intermédiaire du fichier
/etc/ImageMagick-6/policy.xml. En complément, des préventions
supplémentaires sont introduites, y compris quelques vérifications pour les
noms de fichier d'entrée dans les délégués http/https, le retrait complet du
décodeur PLT/Gnuplot et la nécessité de référence explicite dans le nom de
fichier pour les codeurs non sûrs.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 8:6.7.7.10-5+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-486.data"
# $Id: $
