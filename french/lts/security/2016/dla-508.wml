#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes liés ont été découverts dans Expat, un bibliothèque C
d'analyse XML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2012-6702">CVE-2012-6702</a>

<p>Ce problème a été introduit lors de la correction du
<a href="https://security-tracker.debian.org/tracker/CVE-2012-0876">CVE-2012-0876</a>.
Stefan Sørensen a découvert que l'utilisation de la fonction XML_Parse()
ensemençait le générateur de nombres aléatoires en générant des sorties
récurrentes pour les appels de rand().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5300">CVE-2016-5300</a>

<p>C'est le produit d'une solution incomplète pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2012-0876">CVE-2012-0876</a>.
L'analyseur ensemence mal le générateur de nombres aléatoires permettant
à un attaquant de provoquer un déni de service (consommation excessive du
CPU) à l'aide d'un fichier XML avec des identifiants contrefaits.</p></li>

</ul>

<p>Il pourrait être nécessaire de redémarrer manuellement les programmes et
les services utilisant les bibliothèques d'expat.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.1.0-1+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets expat.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-508.data"
# $Id: $
