#use wml::debian::translation-check translation="2c3a538ea007bd4c5a40618a3303a4fdd1c34f42" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans Libvirt, une bibliothèque
d'abstraction de virtualisation, permettant à un client de l'API, doté
uniquement de droits en lecture, d'exécuter des commandes arbitraires au
moyen de l'API virConnectGetDomainCapabilities, ou de lire ou d'exécuter
des fichiers arbitraires au moyen de l'API virDomainSaveImageGetXMLDesc.</p>

<p>En complément, les paramètres cpu-map de libvirt ont été mis à jour pour
faciliter le traitement de
<a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">\
CVE-2018-3639</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-5753">\
CVE-2017-5753</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">\
CVE-2017-5715</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">\
CVE-2018-12126</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">\
CVE-2018-12127</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">\
CVE-2018-12130</a> et <a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">\
CVE-2019-11091</a> par la prise en charge des fonctions md-clear, ssbd,
spec-ctrl et ibpb du processeur lorsqu'on sélectionne des modèles de
processeur sans avoir à se replier sur le tunnel de l'hôte
(« host-passthrough »).</p>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 3.0.0-4+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libvirt.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libvirt, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libvirt">\
https://security-tracker.debian.org/tracker/libvirt</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4469.data"
# $Id: $
