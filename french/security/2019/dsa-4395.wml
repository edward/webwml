#use wml::debian::translation-check translation="13c74272ff7923316d1023584e21f19c0a5f1e92" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le navigateur web
Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17481">CVE-2018-17481</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert
dans la bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5754">CVE-2019-5754</a>

<p>Klzgrad a découvert une erreur dans l'implémentation du protocole de
réseau QUIC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5755">CVE-2019-5755</a>

<p>Jay Bosamiya a découvert une erreur d'implémentation dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5756">CVE-2019-5756</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert
dans la bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5757">CVE-2019-5757</a>

<p>Alexandru Pitis a découvert une erreur de confusion de type dans
l'implémentation du format d'image SVG.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5758">CVE-2019-5758</a>

<p>Zhe Jin a découvert un problème d'utilisation de mémoire après
libération dans blink/webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5759">CVE-2019-5759</a>

<p>Almog Benin a découvert un problème d'utilisation de mémoire après
libération lors du traitement de pages HTML contenant des éléments select.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5760">CVE-2019-5760</a>

<p>Zhe Jin a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5762">CVE-2019-5762</a>

<p>Un problème d'utilisation de mémoire après libération a été découvert
dans la bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5763">CVE-2019-5763</a>

<p>Guang Gon a découvert une erreur de validation d'entrée dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5764">CVE-2019-5764</a>

<p>Eyal Itkin a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5765">CVE-2019-5765</a>

<p>Sergey Toshin a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5766">CVE-2019-5766</a>

<p>David Erceg a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5767">CVE-2019-5767</a>

 <p>Haoran Lu, Yifan Zhang, Luyi Xing et Xiaojing Liao ont signalé une
erreur dans l'interface utilisateur des WebAPK.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5768">CVE-2019-5768</a>

<p>Rob Wu a découvert une erreur d'application de politique dans les
outils de développement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5769">CVE-2019-5769</a>

<p>Guy Eshel a découvert une erreur de validation d'entrée dans
blink/webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5770">CVE-2019-5770</a>

<p>hemidallt a découvert un problème de dépassement de tampon dans
l'implémentation de WebGL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5772">CVE-2019-5772</a>

<p>Zhen Zhou a découvert un problème d'utilisation de mémoire après
libération dans la bibliothèque pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5773">CVE-2019-5773</a>

<p>Yongke Wong a découvert une erreur de validation d'entrée dans
l'implémentation de IndexDB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5774">CVE-2019-5774</a>

<p>Junghwan Kang et Juno Im ont découvert une erreur de validation d'entrée
dans l'implémentation de SafeBrowsing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5775">CVE-2019-5775</a>

<p>evil1m0 a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5776">CVE-2019-5776</a>

<p>Lnyas Zhang a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5777">CVE-2019-5777</a>

<p>Khalil Zhani a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5778">CVE-2019-5778</a>

<p>David Erceg a découvert une erreur d'application de politique dans
l'implémentation des extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5779">CVE-2019-5779</a>

<p>David Erceg a découvert une erreur d'application de politique dans
l'implémentation de ServiceWorker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5780">CVE-2019-5780</a>

<p>Andreas Hegenberg a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5781">CVE-2019-5781</a>

<p>evil1m0 a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5782">CVE-2019-5782</a>

<p>Qixun Zhao a découvert une erreur d'implémentation dans la bibliothèque
JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5783">CVE-2019-5783</a>

<p>Shintaro Kobori a découvert une erreur de validation d'entrée dans les
outils de développement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5784">CVE-2019-5784</a>

<p>Lucas Pinheiro a découvert une erreur d'implémentation dans la
bibliothèque JavaScript v8.</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 72.0.3626.96-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chromium.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de chromium, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4395.data"
# $Id: $
