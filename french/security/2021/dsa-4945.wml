#use wml::debian::translation-check translation="79d3502f9416ddb5b313214ab9f721d43b0d4107" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
webkit2gtk :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21775">CVE-2021-21775</a>

<p>Marcin Towalski a découvert qu'une page web contrefaite pour l'occasion
peut conduire à une possible fuite d'informations et à d'autres corruptions
de mémoire. Pour déclencher la vulnérabilité, une victime doit être
entraînée à visiter une page web malveillante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21779">CVE-2021-21779</a>

<p>Marcin Towalski a découvert qu'une page web contrefaite pour l'occasion
peut conduire à une possible fuite d'informations et à d'autres corruptions
de mémoire. Pour déclencher la vulnérabilité, une victime doit être
entraînée à visiter une page web malveillante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30663">CVE-2021-30663</a>

<p>Un chercheur anonyme a découvert que le traitement de contenus web
contrefaits pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30665">CVE-2021-30665</a>

<p>yangkang a découvert que le traitement de contenus web contrefaits
pourrait conduire à l'exécution de code arbitraire. Apple a été informé
d'un rapport indiquant que ce problème a été activement exploité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30689">CVE-2021-30689</a>

<p>Un chercheur anonyme a découvert que le traitement de contenus web
contrefaits pourrait conduire à une vulnérabilité par script intersite
universel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30720">CVE-2021-30720</a>

<p>David Schutz a découvert qu'un site web malveillant peut être capable
d'accéder à des ports dont l'accès est restreint sur des serveurs
arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30734">CVE-2021-30734</a>

<p>Jack Dates a découvert que le traitement de contenus web contrefaits
 pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30744">CVE-2021-30744</a>

<p>Dan Hite a découvert que le traitement de contenus web contrefaits
pourrait conduire à script intersite universel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30749">CVE-2021-30749</a>

<p>Un chercheur anonyme a découvert que le traitement de contenus web
contrefaits pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30758">CVE-2021-30758</a>

<p>Christoph Guttandin a découvert que le traitement de contenus web
contrefaits pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30795">CVE-2021-30795</a>

<p>Sergei Glazunov a découvert que le traitement de contenus web contrefaits
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30797">CVE-2021-30797</a>

<p>Ivan Fratric a découvert que le traitement de contenus web contrefaits
pourrait conduire à l'exécution de code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30799">CVE-2021-30799</a>

<p>Sergei Glazunov a découvert que le traitement de contenus web contrefaits
pourrait conduire à l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés dans
la version 2.32.3-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4945.data"
# $Id: $
