#use wml::debian::template title="Debian 安装程序" NOHEADER="true"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="6f892e2bb4879338aa7540b6e970f730a9674781"


<h1>新闻</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">更早的新闻</a>
</p>

<h1>使用 Debian 安装程序进行安装</h1>
 

<p>
<if-stable-release release="bullseye">
<strong>要寻找官方的 <current_release_bullseye> 安装媒介和信息</strong>\
，请参阅 \
<a href="$(HOME)/releases/bullseye/debian-installer">bullseye 页面</a>。
</if-stable-release>
<if-stable-release release="bookworm">
<strong>要寻找官方的 <current_release_bookworm> 安装媒介和信息</strong>\
，请参阅 \
<a href="$(HOME)/releases/bookworm/debian-installer">bookworm 页面</a>。
</if-stable-release>
</p>

<div class="tip">
<p>
以下链接指向的映像是用于测试为下个 Debian 发布版本开发的 \
Debian 安装程序的，将会默认安装 Debian testing\
（<q><current_testing_name></q>）。
</p>
</div>

<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet
. -->
<if-testing-installer released="no">
<p>

<strong>要安装 Debian testing</strong>，我们推荐您\
使用安装程序的<strong>每日构建</strong>版本。\
有以下每日构建的映像可用：

</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">
<p>

<strong>要安装 Debian testing</strong>，我们推荐您\
在检查<a href="errata">勘误</a>之后，使用\
安装程序的 <strong><humanversion /></strong> 版本。\
有以下 \
<humanversion /> 的映像可用：

</p>

<h2>官方发布版本</h2>

<div class="line">
<div class="item col50">
<strong>网络安装（通常 180-450 MB）CD 映像</strong>
<netinst-images />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>CD（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>蓝光（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>其他映像（网络安装，[CN:U 盘:][HK:USB 手指:][TW:USB 随身碟:]等)</strong>
<other-images />
</div>
</div>

<p>
或者安装<b>当前 Debian testing 的每周快照</b>，\
安装程序的版本和官方发布版本相同：
</p>

<h2>当前的每周快照</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>CD（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>蓝光（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong>
<devel-full-bd-jigdo />
</div>
</div>

<p>
如果您希望使用最新、最棒的版本，因为您愿意协助我们测试安装程序的\
未来的发布版本，抑或是因为遇到了硬件问题或其他问题，\
您可以尝试以下<strong>每日构建映像</strong>，其中包含了安装程序组件的\
最新版本。
</p>
</if-testing-installer>

<h2>当前的每日快照</h2>

<div class="line">
<div class="item col50">
<strong>网络安装（通常 150-280MB） <!-- and businesscard (generally 20-50 MB) --> CD 映像</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>网络安装 <!-- and businesscard --> CD 映像（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>网络安装，多架构 CD 映像</strong>
<devel-multi-arch-cd />
</div>

<div class="item col50 lastcol">
<strong>其他映像（网络启动、[CN:U 盘:][HK:USB 手指:][TW:USB 随身碟:]等）</strong>
<devel-other-images />
</div>
</div>

# 译者注意: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
如果您的系统的任何硬件的驱动<strong>需要加载非自由固件</strong>\
，您可以使用\
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/">\
常见固件软件包的 tar [CN:文件:][HKTW:档案:]</a>中的一个，或者下载包含\
这些<strong>非自由</strong>固件的<strong>非官方</strong>映像。\
如何使用这些 tar [CN:文件:][HKTW:档案:]，以及\
如何在安装过程中加载固件，请参阅\
<a href="https://d-i.debian.org/doc/installation-guide/en.amd64/ch06s04.html">安装手册</a>。
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/daily-builds/sid_d-i/current/">包含了<strong>非自由</strong>固件的<strong>非官方</strong>映像——每日构建</a>
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/weekly-builds/">包含了<strong>非自由</strong>固件的<strong>非官方</strong>映像——每周构建</a>
</p>
</div>

<hr />

<p>
<strong>注意</strong>
</p>
<ul>
#	<li>Before you download the daily built images, we suggest you check for
#	<a href="https://wiki.debian.org/DebianInstaller/Today">known issues</a>.</li>
	<li>如果某架构的每日映像不能（可靠地）被构建，概览中可能（暂时）缺少该架构的每日映像。</li>
	<li>对于安装映像，校验[CN:文件:][HKTW:档:]（<tt>SHA512SUMS</tt>\
	和 <tt>SHA256SUMS</tt>）可以在映像的同一目录下找到。</li>
	<li>下载完整的 CD 和 DVD 映像，推荐使用 jigdo。</li>
	<li>完整的 DVD 集中只有有限数量的映像可作为 ISO 文件直接下载。\
	大多数用户不需要所有光盘上的所有可用软件，所以为了节省\
	下载服务器和镜像站的空间，完整的映像集只能通过 jigdo 下载。</li>
	<li>多架构<em>网络安装 CD</em> 映像支持 i386/amd64；\
    安装过程与从单架构的网络安装映像安装相似。</li>
</ul>

<p>
<strong>使用 Debian 安装程序之后</strong>，即使没有任何问题，也请发一份\
<a href="https://d-i.debian.org/manual/zh_CN.amd64/ch05s04.html#submit-bug">安装报告</a>给我们。
</p>

<h1>文档</h1>

<p>
<strong>如果您在安装前只想阅读一份文档</strong>，请阅读我们的\
<a href="https://d-i.debian.org/manual/zh_CN.amd64/apa.html">安装指南</a>\
，这是一份安装过程的简要介绍。其他有用的文档包括：
</p>

<ul>
<li>安装手册：\
#    <a href="$(HOME)/releases/stable/installmanual">当前发布版本</a>
#    &mdash;
    <a href="$(HOME)/releases/testing/installmanual">开发版本（测试版）</a>
    &mdash;
    <a href="https://d-i.debian.org/manual/">最新版本（Git）</a>
<br />
详细的安装步骤</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian 安装程序 FAQ</a> 和 <a href="$(HOME)/CD/faq/">Debian CD FAQ</a><br />
常见问题和解答</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian 安装程序 Wiki</a><br />
由[CN:社区:][HKTW:社群:]维护的文档</li>
</ul>

<h1>联系我们</h1>

<p>
<a href="https://lists.debian.org/debian-boot/">debian-boot
邮件列表</a> 是讨论 Debian 安装程序开发工作的主要论坛。
</p>

<p>
我们也有一个 IRC 频道，<tt>irc.debian.org</tt> 的 #debian-boot。这个\
频道主要用于开发，不过偶尔也会用于[CN:支持:][HKTW:支援:]。\
如果您没有收到回复，请尝试使用邮件列表。
</p>
