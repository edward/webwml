#use wml::debian::translation-check translation="282fe47153ee7ae459dbd068bec0e572c214acb8" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i wpa_supplicant og hostapd.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12695">CVE-2020-12695</a>

    <p>Man opdagede at hostapd ikke på korrekt vis håndterede 
    UPnP-tilmeldingsmeddelelser under visse omstændigheder, hvilket gjorde det 
    muligt for en angriber at forårsage et lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-0326">CVE-2021-0326</a>

    <p>Man opdagede at wpa_supplicant ikke på korrekt vis behandlede 
    P2P-gruppeoplysninger (Wi-Fi Direct) fra aktive gruppeejere.  En angriber 
    indenfor radioafstand af enheden der kører P2P, kunne drage nytte af fejlen 
    til at forårsage et lammelsesangreb eller potentielt udføre vilkårlig 
    kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27803">CVE-2021-27803</a>

    <p>Man opdagede at wpa_supplicant ikke på korrekt vis behandlede P2P's provision 
    discovery-forespørgsler (Wi-Fi Direct).  En angriber indenfor radioafstand af 
    enheden der kører P2P, kunne drage nytte af fejlen til at forårsage et 
    lammelsesangreb eller potentielt udføre vilkårlig kode.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2:2.7+git20190128+0c1e29f-6+deb10u3.</p>

<p>Vi anbefaler at du opgraderer dine wpa-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende wpa, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4898.data"
