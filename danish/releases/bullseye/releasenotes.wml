#use wml::debian::template title="Debian 11 -- Udgivelsesbemærkninger" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="ce7e85638a047b969574a7abeb8a634c27b9086b"

<if-stable-release release="stretch">
<p>Dette er en <strong>version under udarbejdelse</strong> af 
Udgivelsesbemærkningerne til Debian 10, med kodenavnet <q>buster</q>, 
som endnu ikke er udgivet.  Oplysningerne heri kan være unøjagtige eller 
forældede, og er sandsynligvis ufuldstændige.
</if-stable-release>

<if-stable-release release="buster">
<p>Dette er en <strong>version under udarbejdelse</strong> af 
Udgivelsesbemærkningerne til Debian 11, med kodenavnet <q>bullseye</q>, 
som endnu ikke er udgivet.  Oplysningerne heri kan være unøjagtige eller 
forældede, og er sandsynligvis ufuldstændige.
</if-stable-release>

<p>For at finde ud af hvad der er af nyheder i Debian 11, se 
Udgivelsesbemærkningerne til din arkitektur:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Udgivelsesbemærkninger'); :>
</ul>

<p>Udgivelsesbemærkningerne indeholder også vejledning til brugere, der 
opgraderer fra tidligere udgivelser.</p>

<p>Hvis din browsers lokaltilpasning er opsat korrekt, kan du anvende 
ovenstående link til automatisk at hente den rette HTML-version &ndash; se
<a href="$(HOME)/intro/cn">indholdsforhandling</a>.  Ellers vælg den nøjagtige
arkitektur, sprog og format i tabellen herunder.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arkitektur</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Sprog</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
