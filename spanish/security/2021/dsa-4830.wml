#use wml::debian::translation-check translation="cc173b8d34b89c7d43e8628759e88ae4a67b7db9"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Simon McVittie descubrió un fallo en el servicio flatpak-portal que puede
permitir que aplicaciones en un entorno aislado («sandboxed») ejecuten código arbitrario en el sistema anfitrión
(fuga de entorno aislado).</p>

<p>El servicio D-Bus Flatpak portal (flatpak-portal, también conocido por su
nombre de servicio D-Bus: org.freedesktop.portal.Flatpak) permite a las aplicaciones de un
entorno aislado Flatpak lanzar sus propios subprocesos en una nueva instancia
de entorno aislado, bien con la misma parametrización de seguridad que el proceso que hace la llamada, o bien
con una parametrización de seguridad más restrictiva. Por ejemplo, esto se utiliza en
navegadores web empaquetados en Flatpak, como Chromium, para lanzar subprocesos
que tratarán contenido web no confiable, dando a esos subprocesos un
entorno aislado más restrictivo que el del propio navegador.</p>

<p>En las versiones vulnerables, el servicio Flatpak portal pasa variables de entorno
especificadas por quien hace la llamada a procesos del sistema anfitrión no pertenecientes a entornos aislados
y, en particular, a la orden flatpak run, que se utiliza para lanzar la
instancia del nuevo entorno aislado. Una aplicación Flatpak maliciosa o comprometida podría establecer
variables de entorno en las que confía la orden flatpak run y
usarlas para ejecutar código arbitrario fuera del entorno aislado.</p>

<p>Para la distribución «estable» (buster), este problema se ha corregido en
la versión 1.2.5-0+deb10u2.</p>

<p>Le recomendamos que actualice los paquetes de flatpak.</p>

<p>Para información detallada sobre el estado de seguridad de flatpak, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/flatpak">\
https://security-tracker.debian.org/tracker/flatpak</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4830.data"
