#use wml::debian::template title="Uso de Git para trabajar en el sitio web de Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="1c96e54427d682f8dbb97fc1f4b6b9afca0ab042"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#work-on-repository">Trabajar con el repositorio Git</a></li>
<li><a href="#write-access">Acceso de escritura al repositorio Git</a></li>
<li><a href="#notifications">Recibir notificaciones</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> <a href="https://git-scm.com/">Git</a> es un <a href="https://en.wikipedia.org/wiki/Version_control">sistema 
de control de versiones</a> que ayuda a coordinar el trabajo de varios desarrolladores. Cada usuario puede tener una copia local del repositorio
principal. Las copias locales pueden estar en la misma máquina o repartidas por todo el mundo.
Los desarrolladores pueden modificar la copia local y confirmar los cambios en el repositorio principal cuando estén listos.</p>
</aside>

<h2><a id="work-on-repository">Trabajar con el repositorio Git</a></h2>

<p>
Entremos directamente en materia. En esta sección aprenderá cómo hacer una copia local del repositorio principal,
cómo mantenerla actualizada y cómo enviar su trabajo.
También explicaremos cómo trabajar en las traducciones.
</p>

<h3><a name="get-local-repo-copy">Obtención de una copia local</a></h3>

<p>
Primero instale Git. A continuación 
configure Git e introduzca su nombre y su dirección de correo electrónico.
Si no ha trabajado antes con Git, probablemente sea buena idea que empieze por consultar la documentación general de Git.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/doc">Documentación de Git</a></button></p>

<p>
El siguiente paso es clonar el repositorio (es decir: hacer una copia local del mismo).
Hay dos maneras de hacerlo:
</p>

<ul>
  <li>Registre una cuenta en <url https://salsa.debian.org/> y habilite el acceso SSH subiendo
  una clave SSH pública a su cuenta. Consulte las <a 
  href="https://salsa.debian.org/help/ssh/index.md">páginas de ayuda
  de Salsa</a> para más detalles. Después puede clonar el
  repositorio <code>webwml</code> con la orden:
<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>
  </li>
  <li>Alternativamente, puede
  clonar el repositorio utilizando el protocolo HTTPS. Tenga en cuenta que
  utilizando este método creará el repositorio local, pero no
  podrá enviar modificaciones directamente al repositorio:
<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>
  </li>
</ul>

<p>
<strong>Un consejo:</strong> Clonar el repositorio <code>webwml</code> completo conlleva la descarga de unos
1,3 GB de datos, lo que podría ser demasiado si utiliza una conexión
a Internet lenta o inestable. Si es este su caso, puede definir
una profundidad mínima para que la descarga inicial sea menor:
</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Tras obtener un repositorio (superficial) utilizable, puede ir aumentando la profundidad de
la copia local y, finalmente, convertirla en un repositorio local
completo:</p>

<pre>
  git fetch --deepen=1000 # aumenta la profundidad del repo con otros 1000 commits
  git fetch --unshallow   # descarga todos los commits que faltan, convierte el repo en un repo completo
</pre>

<p>También puede extraer solo un subconjunto de las páginas:</p>

<ol>
  <li><code>git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git</code></li>
  <li><code>cd webwml</code></li>
  <li><code>git config core.sparseCheckout true</code></li>
  <li>Cree el fichero <code>.git/info/sparse-checkout</code> dentro del directorio
  <code>webwml</code> para definir el contenido que quiere extraer. Por ejemplo,
  si solo quiere extraer los ficheros base, las páginas en inglés y las traducciones a catalán y español,
  el fichero tendrá este aspecto:
    <pre>
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
    </pre></li>
  <li>Por último, puede crear la copia de trabajo con: <code>git checkout --</code></li>
</ol>

<h3><a name="submit-changes">Envío de cambios locales</a></h3>

<h4><a name="keep-local-repo-up-to-date">Mantener actualizado su repo local</a></h4>

<p>Cada pocos días (y, desde luego, ¡antes de empezar a modificar algo!)
debería ejecutar:</p>

<pre>
  git pull
</pre>

<p>para descargar los ficheros del repositorio que hayan cambiado.</p>

<p>
Le recomendamos encarecidamente que tenga su directorio de trabajo Git local limpio
antes de ejecutar <code>git pull</code> y empezar a editar ficheros.
Si tiene cambios sin confirmar o cambios confirmados localmente en la rama actual pero no incluidos
en el repositorio remoto, la ejecución de <code>git pull</code> creará automáticamente
commits de fusión o incluso fallará debido a conflictos. Considere mantener su
trabajo en curso en otra rama o utilizar órdenes como <code>git stash</code>.
</p>

<p>Nota: Git es un sistema de control de versiones distribuido (no
centralizado). Esto significa que cuando confirma cambios, estos cambios solo se
almacenan en su repositorio local. Para compartirlos con otros, además,
tiene que enviar sus cambios al repositorio central alojado en Salsa.</p>

<h4><a name="example-edit-english-file">Ejemplo: edición de ficheros</a></h4>

<p>
Veamos un ejemplo práctico de una típica sesión de edición.
Supondremos que ya ha obtenido una <a href="#get-local-repo-copy">copia local</a> del repo 
con <code>git clone</code>. Los pasos siguientes serían:
</p>

<ol>
  <li><code>git pull</code></li>
  <li>Ahora puede empezar a editar y hacer cambios a los ficheros.</li>
  <li>Cuando haya terminado, confirme los cambios en su repositorio local:
    <pre>
    git add ruta/al(os)/fichero(s)
    git commit -m "Su mensaje para este commit"
    </pre></li>
  <li>Si tiene <a href="#write-access">acceso ilimitado de escritura</a> al repositorio <code>webwml</code> remoto,
  ahora puede enviar directamente los cambios al repo de Salsa: <code>git push</code></li>
  <li>Si no tiene acceso directo de escritura al repositorio <code>webwml</code>, 
  envíe los cambios mediante una <a href="#write-access-via-merge-request">solicitud de fusión («merge request»)</a> o póngase en contacto con otros desarrolladores y pídales ayuda.</li>
</ol>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/docs/gittutorial">Documentación de Git</a></button></p>

<h4><a name="closing-debian-bug-in-git-commits">Cierre de fallos de Debian en commits de Git</a></h4>

<p>
Si incluye el texto <code>Closes: #</code><var>nnnnnn</var> en la entrada del historial de confirmaciones correspondiente a
su commit, el fallo número <code>#</code><var>nnnnnn</var> se cerrará
cuando envíe los cambios. El formato preciso es el mismo que
<a href="$(DOC)/debian-policy/ch-source.html#id24">en las normas de Debian</a>.</p>

<h4><a name="links-using-http-https">Uso de HTTP/HTTPS en los enlaces</a></h4>

<p>Muchos sitos web de Debian soportan SSL/TLS. Use enlaces HTTPS cuando sea
posible y razonable. <strong>Sin embargo</strong>, algunos
sitios web de Debian/DebConf/SPI/etc, o bien no tienen soporte HTTPS,
o bien utilizan solo la autoridad certificadora de SPI (y no una autoridad certificadora SSL en la que confíen todos los navegadores). Para
evitar que los usuarios que no utilizan Debian obtengan mensajes de error, no incluya enlaces
a esos sitios utilizando HTTPS.</p>

<p>El repositorio Git rechazará tanto los commits que contengan enlaces HTTP
a los sitios web de Debian que soportan HTTPS como los que contengan enlaces HTTPS a
los sitios web de Debian/DebConf/SPI que se sabe que, o bien no soportan
HTTPS, o bien usan certificados firmados únicamente por SPI.</p>

<h3><a name="translation-work">Trabajo con traducciones</a></h3>

<p>Las traducciones deberían mantenerse siempre actualizadas con relación al
fichero en inglés correspondiente. La cabecera <code>translation-check</code> en los ficheros
traducidos se usa para indicar en qué versión del fichero en inglés está basado
el fichero traducido actual. Si modifica ficheros traducidos, tiene que actualizar la
cabecera <code>translation-check</code> para que coincida con el hash del commit Git de la
modificación correspondiente en el fichero en inglés. Puede obtener dicho hash con la
orden:</p>

<pre>
  git log ruta/al/fichero/en/inglés
</pre>

<p>Si hace una traducción nueva de un fichero, utilice el script <code>copypage.pl</code>.
Este script crea una plantilla para su idioma, incluyendo la cabecera
de traducción correcta.</p>

<h4><a name="translation-smart-change">Modificación de traducciones con smart_change.pl</a></h4>

<p><code>smart_change.pl</code> es un script diseñado para facilitar
la actualización, de forma conjunta, de ficheros originales y de sus traducciones. Hay
dos maneras de usarlo, dependiendo de los cambios que esté haciendo.</p>

<p>
Esta es la forma de utilizar <code>smart_change.pl</code> y de actualizar las cabeceras
<code>translation-check</code> cuando está trabajando manualmente en los ficheros:
</p>

<ol>
  <li>Modifique el fichero o ficheros originales y confirme los cambios.</li>
  <li>Actualice las traducciones.</li>
  <li>Ejecute <code>smart_change.pl -c COMMIT_HASH</code> (use el hash del commit de los cambios en el fichero o ficheros originales). 
  El script recogerá los cambios y actualizará
  las cabeceras de los ficheros traducidos.</li>
  <li>Revise los cambios (por ejemplo, con <code>git diff</code>).</li>
  <li>Confirme los cambios de las traducciones.</li>
</ol>

<p>
Alternativamente, puede utilizar expresiones regulares para hacer varios cambios en ficheros en una única pasada:
</p>

<ol>
  <li>Ejecute <code>smart_change.pl -s s/FOO/BAR/ fich-orig-1 fich-orig-2 ...</code></li>
  <li>Revise los cambios (por ejemplo, con <code>git diff</code>).</li>
  <li>Confirme los cambios en el fichero o ficheros originales.</li>
  <li>Ejecute <code>smart_change.pl fich-orig-1 fich-orig-2</code>
    (esta vez <strong>sin la expresión regular</strong>). Ahora
    solo actualizará las cabeceras de los ficheros traducidos.</li>
  <li>Finalmente, confirme los cambios de las traducciones.</li>
</ol>

<p>
Este segundo ejemplo requiere algo más de esfuerzo que el primero ya que supone hacer dos commits, pero
resulta inevitable debido a la manera en que funcionan los hashes de los commits en Git.
</p>

<h2><a id="write-access">Acceso de escritura al repositorio Git</a></h2>

<p>
El código fuente del sitio web de Debian se gestiona con Git y se encuentra alojado en
<url https://salsa.debian.org/webmaster-team/webwml/>. 
Por omisión, no se permite que los visitantes envíen cambios al repositorio del código fuente.
Si quiere contribuir al sitio web de Debian, necesita algún tipo de permiso
para tener acceso de escritura al repositorio.
</p>

<h3><a name="write-access-unlimited">Acceso ilimitado de escritura</a></h3>

<p>
Si necesita acceso ilimitado de escritura al repositorio (por ejemplo, si va a
contribuir con frecuencia), solicite acceso de escritura a través de
la interfaz web <url https://salsa.debian.org/webmaster-team/webwml/> tras
identificarse en la plataforma Salsa de Debian.
</p>

<p>
Si no ha participado antes en el desarrollo del sitio web de Debian y no tiene experiencia previa,
envíe también un correo electrónico a <a href="mailto:debian-www@lists.debian.org">
debian-www@lists.debian.org</a> presentándose antes de solicitar
acceso ilimitado de escritura. Si es tan amable cuéntenos algo sobre usted como, por ejemplo,
en qué parte del sitio web tiene pensado trabajar, qué idiomas habla
y si hay algún miembro de un equipo Debian que pueda responder por usted.
</p>

<h3><a name="write-access-via-merge-request">Solicitudes de fusión («Merge Requests»)</a></h3>

<p>
No es necesario obtener acceso ilimitado de escritura al repositorio,
siempre puede realizar una solicitud de fusión para que otros
desarrolladores revisen y acepten su trabajo. Siga el procedimiento estándar
para las solicitudes de fusión proporcionado por la plataforma GitLab de Salsa a través de
su interfaz web y consulte estos dos documentos:
</p>

<ul>
  <li><a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Project forking workflow [«Flujo de trabajo de bifurcaciones de proyectos»]</a></li>
  <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">When you work in a fork [«Cuando trabaja en una bifurcación»]</a></li>
</ul>

<p>
Tenga en cuenta que no todos los desarrolladores del sitio web monitorizan las solicitudes de fusión. En consecuencia, podría
pasar un tiempo antes de que tenga noticias. Si tiene dudas sobre si su
contribución será aceptada o no, puede enviar un correo electrónico a la lista de correo
<a href="https://lists.debian.org/debian-www/">debian-www</a>
y pedir una revisión de su solicitud.
</p>

<h2><a id="notifications">Recibir notificaciones</a></h2>

<p>
Si está trabajando en el sitio web de Debian, es posible que quiera estar al tanto de la actividad en el repositorio <code>webwml</code>. Hay dos herramientas para mantenerse informado: notificaciones de confirmaciones y notificaciones de solicitudes de fusión.
</p>

<h3><a name="commit-notifications">Recibir notificaciones de las confirmaciones</a></h3>

<p>Hemos configurado el proyecto <code>webwml</code> en Salsa de forma que las confirmaciones se
muestren en el canal IRC #debian-www.</p>

<p>Si quiere recibir notificaciones por correo electrónico
sobre confirmaciones en el repo de <code>webwml</code>, suscríbase al pseudopaquete
<code>www.debian.org</code> a través de tracker.debian.org y active allí la palabra clave
<code>vcs</code> siguiendo estos pasos (solo una vez):</p>

<ol>
  <li>Abra un navegador web y vaya a <url https://tracker.debian.org/pkg/www.debian.org>.</li>
  <li>Suscríbase al pseudopaquete <code>www.debian.org</code>. (Puede identificarse
      mediante SSO o registrar una dirección de correo electrónico y una contraseña si no está usando ya
      tracker.debian.org para otros fines).</li>
  <li>Vaya a <url https://tracker.debian.org/accounts/subscriptions/>, después a <code>modify
      keywords</code>, marque <code>vcs</code> (si no está ya marcado) y grabe los cambios.</li>
  <li>A partir de este momento recibirá correos electrónicos cuando alguien confirme cambios en el
      repo <code>webwml</code>.</li>
</ol>

<h3><a name="merge-request-notifications">Recibir notificaciones de las solicitudes de fusión</a></h3>

<p>
Si desea recibir correos electrónicos de notificación cada vez que se realice una nueva
solicitud de fusión en el repositorio <code>webwml</code> de Salsa, puede modificar
la configuración de notificaciones en la interfaz web siguiendo estos pasos:
</p>

<ol>
  <li>Identifíquese en Salsa y vaya a la página del proyecto.</li>
  <li>Haga click en el icono con la campana en la parte superior de la página inicial del proyecto.</li>
  <li>Seleccione el nivel de notificación que prefiera.</li>
</ol>
