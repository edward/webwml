#use wml::debian::template title="Debian in olandese"
#use wml::debian::translation-check translation="e1086a4582af6d8098ee177d56337deffe7a389d" maintainer="Giuseppe Sacco"

# This contents of this page is completely the responsibility of
# the translation team

<h1>Debian nelle nazioni e territori dove si parla olandese</h1>

<p>Debian vuole essere un sistema operativo universale.
Questo implica che Debian non solo funzioni su svariate architetture, ma
che cerca anche di essere disponibile nel maggior numero di lingue
e per il maggior numero di persone. Non è quindi sorprendente che Debian,
inizialmente sviluppata negli USA e in inglese come lingua principale, sia
evoluta in una rete globale di volontari e abbia utenti in tutto il mondo.</p>
<p>Nelle nazioni e nei territori del mondo dove si parla olandese ci sono
svariati sviluppatori Debian oltre e molti altri contributori che offrono
il loro supporto, traducendo il sistema operativo in olandese
o convertendo le pagine web Debian in modo che chi parla olandese possa
leggerle nella propria lingua madre.</p>

<h2>Debian per gli utenti che parlano olandese</h2>

<ul>
  <li><a href="$(HOME)/social_contract">Il <q>contratto sociale</q> Debian
      con la Free Software Community</a></li>
  <li><a href="$(HOME)/intro/cn">Leggere le pagine web Debian nella propria lingua</a></li>
  <li>liste di messaggi in olandesi per utenti Debian:
    <a href="https://lists.debian.org/debian-user-dutch/">archivi della lista di messaggi</a>;
    <a href="https://lists.debian.org/debian-user-dutch/">iscrizione alla lista</a>
  </li>
</ul>

<h2>Aiutare con la traduzione di Debian</h2>

<p>Abbiamo l'ambizione di rendere disponibile in olandese sia il sistema
operativo Debian che la sua documentazione.
Può essere di grande aiuto per le persone che non hanno grande familiarità
con l'inglese. In particolar modo per i bambini, la mancanza di una
traduzione in olandese può essere una barriera importante.</p>
<p>Come per lo sviluppo dello stesso sistema operativo Debian, così la
sua traduzione si basa sul volontariato. Se si ha del tempo disponibile e
una padronanza sufficiente della lingua inglese, si potrebbe prendere
in considerazione l'idea di partecipare. Se quest'idea risulta attraente
si possono trovare maggiori informazioni qui:</p>

<ul>
  <li>Sul <a href="dutch/index.html">sito web</a> del progetto di traduzione
  di Debian si trovano informazioni aggiuntive sulla traduzione di Debian
  in olandese.</li>
  <li>La traduzione in olandese è coordinata in questa lista:
  <a href="mailto:debian-l10n-dutch@lists.debian.org">
  debian-l10n-dutch@lists.debian.org</a>
  (<a href="https://lists.debian.org/debian-l10n-dutch/">pagine per
  iscrizione e archivi</a>).</li>
</ul>
